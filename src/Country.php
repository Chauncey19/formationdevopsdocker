<?php

class Country
{
  public function getCountryNameByCityName($city) {
    $eu_cities = array('New York', 'Miami');

    if (in_array($city, $eu_cities)) {
      $country = 'EU';
    } else {
      $country = 'Unknow';
    }
    return $country;
  }
}